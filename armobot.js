var interactive=true

var five = require("johnny-five");
var EtherPortClient = require("etherport-client").EtherPortClient;

var polar={}
polar.lat=45
polar.lng=90
polar.r=12

var M=Math

var param={
pwm:{
address:'0x40',controller:'PCA9685'},
dimension:{avBras:10,bras:10.5}
};


var dimension=param.dimension;

var s={};

//var board = new five.Board();
var board = new five.Board({
  port: new EtherPortClient({
    host: "192.168.1.4",  // IP ESP8266
    port: 3030                
  }),
  timeout: 10000,
  repl: false
});

board.on("ready", function() {
  console.log("Connected");


  s.pivot=new five.Servo({
    pin:0,
    controller:param.pwm.controller,
    address:param.pwm.address,
    startAt:90,
    range:[20,160],
    custom:{thOffset:-5}
  })

  s.epaule=new five.Servo({
    pin:1,
    controller:param.pwm.controller,
    address:param.pwm.address,
    startAt:150,
    range:[20,175],
    custom:{thOffset:-20}
  })

  s.coude=new five.Servo({
    pin:2,
    controller:param.pwm.controller,
    address:param.pwm.address,
    startAt:50,
    range:[5,160],
    custom:{thOffset:30}
  })
    

   if(interactive){

        var keypress = require('keypress');
        keypress(process.stdin);

        process.stdin.on('keypress', function (ch, key) {
          console.log('got "keypress"', key.name);
          if (key && key.ctrl && key.name == 'c') {
                process.exit()
            process.stdin.pause();
          }else if (key.name=='left'){
                move(s.pivot,1)
            }
            else if (key.name=='right'){
                move(s.pivot,-1)
            }
            else if (key.name=='up'){
                move(s.coude,1)
            }
            else if (key.name=='down'){
                move(s.coude,-1)
            }
            else if (key.name=='s'){
                move(s.epaule,1)
            }
            else if (key.name=='z'){
                move(s.epaule,-1)
            }
            else if (key.name=='t'){
                servoTo(s.coude,90)
            }
            else if (key.name=='y'){
                toPolar(polar.r+0.5,45,90)
            }
            else if (key.name=='h'){
                toPolar(polar.r-0.5,45,90)
            }
            else if (key.name=='u'){
                servoTo(s.pivot,90)
            }
        });

        process.stdin.setRawMode(true);
        process.stdin.resume();
    }



})

function move(servo,dir){
    newPosition=servo.value+(5*dir)
    servo.to(newPosition,100)
    console.log(servo.value+servo.custom.thOffset)
    updateCoord()
}
    
function servoTo(servo,angle){
    servo.to(angle-servo.custom.thOffset,200)
}

function toPolar(r,lat,lng){
    r=constrain(r,1,param.dimension.bras+param.dimension.avBras)
    polar.r=r
    var t=polarToAngle(r,lat,lng)
    servoTo(s.coude,t.j);
    servoTo(s.epaule,t.i);
    servoTo(s.pivot,t.k);
    console.log(r)
    console.log(lat)
    console.log(lng)
    console.log('--')

}

function thAngle(servo){
    return servo.value+servo.custom.thOffset
}

//Update coordinate accord to angles
function updateCoord(){ //TODO
    var coude=thAngle(s.coude)
    var epaule=thAngle(s.epaule)
    r=M.sqrt(M.pow(dimension.bras,2)+M.pow(dimension.avBras,2)-2*dimension.bras*dimension.avBras*M.cos(rad(coude)))
    //lat=(M.sqrt(M.pow(r,2)+M.pow(dimension.avBras,2)-2*r*dimension.avBras*M.cos(rad(epaule))))
    lat=epaule-degrees(M.acos((M.pow(dimension.avBras,2)+M.pow(r,2)-M.pow(dimension.bras,2))/(2*dimension.avBras*r)))
    console.log('r: '+r)
    console.log('lat :'+lat)
}
//Math


//@param {Number} r: Distance between origine and effector
//@param {Number} lat: Latitude
//@param {Number} lng: Longitude
function polarToAngle(r,lat,lng){ //j = coude, i=epaule, k=base
    bras=10
    avBras=10.5
    j=M.acos((M.pow(bras,2)+M.pow(avBras,2)-M.pow(r,2))/(2*bras*avBras))
    j=degrees(j)
    
    i=M.acos((M.pow(avBras,2)+M.pow(r,2)-M.pow(bras,2))/(2*avBras*r))
    i=lat+(degrees(i))
    
    k=lng
    return {'i':i,'j':j,'k':k}
}

function cartToPolar(x,y,z){
    r=M.sqrt(M.pow(x,2)+M.pow(y,2))
    lat=degrees(M.atan(z/x))
    lng=degrees(M.atan(y/x))
    return {'r':r,'lat':lat,'lng':lng}
}

function degrees(rad){
    return rad * 180 / Math.PI
}

function rad(degrees){
    return degrees * Math.PI / 180
}

function constrain (value, lower, upper) {
  return Math.min(upper, Math.max(lower, value));
};






