var M=Math
function polarToAngle(r,lat,lng){ //j = coude, i=epaule, k=base
    bras=10
    avBras=10
    j=M.acos((M.pow(bras,2)+M.pow(avBras,2)-M.pow(r,2))/(2*bras*avBras))
    j=degrees(j)
    
    i=M.acos((M.pow(avBras,2)+M.pow(r,2)-M.pow(bras,2))/(2*avBras*r))
    i=lat+(degrees(i))
    
    k=lng
    return {'i':i,'j':j,'k':k}
}

function degrees(rad){
    return rad * 180 / Math.PI
}

function cartToPolar(x,y,z):
    r=M.sqrt(M.pow(x,2)+M.pow(y,2))
    lat=degrees(M.atan(z/x))
    lng=degrees(M.atan(y/x))
    return {'r':r,'lat':lat,'lng':lng}
