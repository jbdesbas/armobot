from math import *

bras=10
avBras=10.5


"""
def polarToAngle(c,z): #i avantbras/bras #h bras/epaule
    i=acos((pow(a,2)+pow(b,2)-pow(c,2))/(2*a*b))
    i=degrees(i)
    h1=acos((pow(b,2)+pow(c,2)-pow(a,2))/(2*b*c))
    h1=degrees(h1)
    h1=90+h1
    
    #cp=sqrt(pow(c,2)-pow(z,2))
    h2=asin(z/c)
    h2=degrees(h2)
    return {'h1':h1,'i':i,'h2':h2}
"""

#r = distance, lat=latitude[-90;90], lng=longitude[-180;180]
def polarToAngle(r,lat,lng): #j = coude, i=epaule, k=base
    j=acos((pow(bras,2)+pow(avBras,2)-pow(r,2))/(2*bras*avBras))
    j=degrees(j)
    
    i=acos((pow(avBras,2)+pow(r,2)-pow(bras,2))/(2*avBras*r))
    i=lat+(degrees(i))
    
    k=lng
    return {'i':i,'j':j,'k':k}

def cartToPolar(x,y,z):
    r=sqrt(pow(x,2)+pow(y,2))
    lat=degrees(atan(z/x))
    lng=degrees(atan(y/x))
    return {'r':r,'lat':lat,'lng':lng}
    

